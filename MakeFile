all: add
 
first: add.o
	gcc -o $@ $+
 
first.o : first.s
	as -o $@ $<
 
clean:
	rm -vf first *.o
